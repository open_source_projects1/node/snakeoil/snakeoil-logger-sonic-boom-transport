import SonicBoom from "sonic-boom"
import LoggerTransport from "snakeoil-logger/src/LoggerTransport.js"

class SonicBoomTransport extends LoggerTransport {

    // https://www.npmjs.com/package/sonic-boom
    constructor(options, transportOptions = {lineBreak : "\r\n"}) {
        super(options)
        if(typeof this._options.fd === "function"){
            this.fd = this._options.fd
            this._options.fd = this.fd().fd
        }

        this.sonic = new SonicBoom(this._options);
        this._transportOptions = transportOptions
        if(!this._transportOptions.lineBreak){
            this._transportOptions.lineBreak = "\r\n"
        }
    }

    write(any){
        // check if file descriptor changed in the mean time
        // this method will hit the cache
        if(this.fd().fd !== this._options.fd){
            this._options.fd = this.fd().fd
            this.sonic.flushSync()
            this.sonic = new SonicBoom(this._options)
        }

        if(typeof any !== "string") throw new Error("input: '"+ any +"' was not string")
        this.sonic.write(any + this._transportOptions.lineBreak)
    }
}


export default SonicBoomTransport